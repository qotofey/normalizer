package normalizer

import (
	"reflect"
	"testing"
)

func TestRemovePunctuation(t *testing.T) {
	// убрать все знаки припенания кроме пробелов
	var tests = []struct{
		text string
		want string
	}{
		{"", ""},
		{"?! .", ""},
		{"К.Маркс. Капитал. Критика полит. экономии.", "К Маркс Капитал Критика полит экономии"},
		{"Д.Письменный. Конспект лекций по высшей математике.", "Д Письменный Конспект лекций по высшей математике"},
	}

	for _, test := range tests {
		input := test.text
		RemovePunctuation(&test.text)
		if test.text != test.want {
			t.Errorf("RemovePunctuation(%q) == %q, want %q", input, test.text, test.want)
		}
	}
}

func TestTransliteration(t *testing.T) {
	var tests = []struct{
		text string
		want string
	}{
		{"", ""},
		//{"привет", "privet"},
	}

	for _, test := range tests {
		input := test.text
		Transliterate(&test.text)
		if test.text != test.want {
			t.Errorf("Transliterate(%q) == %q, want %q", input, test.text, test.want)
		}
	}
}


// 1 - привести текст в нижний регистр
// 2 - заменить буквы ё на е
func TestSimplify(t *testing.T) {
	var tests = []struct{
		text string
		want string
	}{
		{" TeXT, ТеКст .! ", " text, текст .! "},
		{"Богатство Обществ", "богатство обществ"},
		{"Ruby On Rails", "ruby on rails"},
		{"К.Маркс. Капитал. Критика полит. экономии.", "к.маркс. капитал. критика полит. экономии."},
		{"еёеёеёеёе", "еееееееее"},
	}

	for _, test := range tests {
		input := test.text
		Simplify(&test.text)
		if test.text != test.want {
			t.Errorf("Simplify(%q) == %q, want %q", input, test.text, test.want)
		}
	}
}

// разбить текст на слова
func TestSplitIntoWords(t *testing.T) {
	var tests = []struct{
		text string
		want []string
	}{
		{"", []string {}},
		{"text текст", []string {"text", "текст"}},
		{"text\t\t \rтекст", []string {"text", "текст"}},
		{"кто-нибудь из-за", []string {"кто-нибудь", "из-за"}},
	}

	for _, test := range tests {
		got := SplitIntoWords(test.text)
		if !reflect.DeepEqual(got, test.want)  {
			t.Errorf("SplitIntoWords(%q) == %q, want %q", test.text, got, test.want)
		}
	}
}