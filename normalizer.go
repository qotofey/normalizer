package normalizer

import (
	"regexp"
	"strings"
)

/* 
 * удаляет все знаки припинания кроме пробелов
 * удаляет все лишние пробелы (между двумя словами должно быть не более одного пробела)
 */
func RemovePunctuation(text *string) {
	punctuationsRx := regexp.MustCompile(`[^а-яА-ЯёЁa-zA-Z\d]`)
	*text = punctuationsRx.ReplaceAllString(*text, " ")

	spacesRx:= regexp.MustCompile(`\s{2,}`)
	*text = spacesRx.ReplaceAllString(*text, " ")

	*text = strings.TrimSpace(*text)
}

/*
 * приводит весь текст к нижмнему регистру
 * заменяет все буквы "ё" на "е"
 */
func Simplify(text *string) {
	*text = strings.ToLower(*text)
	*text = strings.ReplaceAll(*text, "ё", "е")
}

/*
 * заменяет кириллические символы на латинские
 * 	а - a				к - k				х - kh
 * 	б - b				л - l				ц - ts
 * 	в - v				м - m				ч - ch
 * 	г - g				н - n				ш - sh
 * 	д - d				о - o				щ - shch
 * 	е - e				п - p				ъ - ie
 * 	ё - e				р - r				ы - y
 * 	ж - zh				с - s				ь -
 * 	з - z				т - t				э - e
 * 	и - i				у - u				ю - iu
 * 	й - i				ф - f				я - ia
 *
 */
func Transliterate(text *string) {

}

// делит предложение на слова
func SplitIntoWords(text string) []string {
	return strings.Fields(text)
}
